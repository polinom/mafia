
GameMethods = {
	tottal_players: function(){
		return parseInt(this.sheriffs) + parseInt(this.doctors) + parseInt(this.citizens) + parseInt(this.mafias);
	},

    players: function(){
        return Players.find({code: this.code});
    }
};


Games = new Meteor.Collection("games", {
	transform: function(doc){
        return _.defaults(doc, GameMethods);
	}
});

Players = new Meteor.Collection('players');

Players.alive_players = function(game_code){
    return this.find({code: game_code, alive: true});
};

Players.ready_vote_players = function(game_code){
    return this.find({code: game_code, alive: true, ready_vote: true});
};

Players.all_ready_vote = function(game_code){
    return this.ready_vote_players(game_code).count() === this.alive_players(game_code).count();
};

Players.ready_night_players = function(game_code){
    return this.find({code: game_code, alive: true, ready_night: true});
};

Players.all_ready_night = function(game_code){
    return this.ready_night_players(game_code).count() === this.alive_players(game_code).count();
};

Players.voted_players = function(game_code){
    return this.ready_night_players(game_code).count() === this.alive_players(game_code).count();
};

Players.voted_day_players = function(game_code){
   return this.find({code: game_code, 'vote.type': 'day' } );
};

Players.is_all_voted_day = function(game_code){
    return this.alive_players(game_code).count() === this.voted_day_players(game_code).count();
};

Players.after.insert(function(userId, player){
    var game = Games.findOne({code: player.code});
    var tottal = game.tottal_players();
    var count_players = Players.find({code: player.code}).count();
    if (tottal === count_players){
        Meteor.call('assign_roles', game);
        Games.update({_id: game._id}, {$set: {game_state: 'card'}});
    }
});