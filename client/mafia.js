

Meteor.Router.add({
  '/':   'initial',
  '/start': 'start',
  '/code': 'game_code',
  '/game': 'game'
});


Template.start.events({
  'click input#begin': function (e, t) {
      var citizens = t.find('#citizens');
      var mafias = t.find('#mafias');
      var doctors = t.find('#doctors');
      var sheriffs = t.find('#sheriffs');
      Meteor.call('create_game',
                  citizens.value,
                  mafias.value,
                  doctors.value,
                  sheriffs.value,
                  function (error, code) {
                    Session.set('game_code', code);
                    Meteor.Router.to('/code');
                }
      );
  }
});


Template.game_code.events({
  'click a#join': function (e, t) {
    var code;
    if (!Session.get('game_code')){
        code = t.find('input#game_code').value;
        var game = Games.findOne({code: code});
        if (game.tottal_players() === Players.find({code:code})){
          Session.set('form_error', "Room is full!");
        } else {
          Session.set('game_code', code);
        }
    } else {
      code = Session.get('game_code');
    }
    if (!Session.get('player_name')){
        var name = t.find('input#player_name');
        Session.set('player_name', name.value);
        var player_id = Players.insert({
                                     name: name.value,
                                     code: code,
                                     alive: true,
                                     ready_vote:false,
                                     ready_nigh: false,
                                     voters_list: []
                                   });
        Session.set('player_id', player_id);
    }

    if (Session.get('player_name') && Session.get('player_name')){
      Meteor.Router.to('/game');
    }
  }
});

Template.waiting.players = function(){
  var code = Session.get('game_code');
  return Players.find({code: code });
};

Template.game.game = function(){
  return Games.findOne({code: Session.get('game_code')});
};

Template.card.player = function(){
  return Players.findOne({_id: Session.get('player_id')});
};

Template.results.player = function(){
  return Players.findOne({_id: Session.get('player_id')});
};

Template.results.killed_now_person = function(){
  return Players.findOne({code: Session.get('code_game'), alive: false },  {sort: {time_killed: -1} });
};

Template.card.events({
  'click a#show_hide': function (e, t) {
    if (Session.get('show_card')) {
      Session.set('show_card', false);
    } else {
       Session.set('show_card', true);
    }
  },

  'click a#vote': function(e, t){
    var val;
    if (Template.card.player().ready_vote){ val = false; } else { val = true; }
    Players.update({_id: Session.get('player_id')}, {$set: {ready_vote: val}});
    if (val === true){ Meteor.call('check_all_vote_ready', Session.get('game_code')); }
  },

  'click a#night': function(e, t){
    var val;
    if (Template.card.player().ready_night){ val = false; } else { val = true; }
    Players.update({_id: Session.get('player_id')}, {$set: {ready_night: val}});
    if (val === true){ Meteor.call('check_all_night_ready', Session.get('game_code')); }
  }
});

Template.card.player = function(){
  return Players.findOne({_id: Session.get('player_id')});
};

Template.vote.people_for_vote = function(){
  return Players.find({code: Session.get('game_code'), alive: true, _id: {$ne: Session.get('player_id') }});
};

Template.choose_person.events({
  'click .person': function (e, t) {
   var person = t.find('a.person');
   var person_id = $(person).attr('id');
   if (!Session.get('voted')){
       Players.update({_id: Session.get('player_id')}, {$set: {'vote': {'person_id': person_id, type: 'day' }}});
       Session.set('voted', {person_id: person_id});
       Players.update({_id: person_id}, {$push: {voters_list: Session.get('player_name')} } );
       Meteor.call('check_all_voted_day', Session.get('player_id'));
   }
  }
});


Template.choose_person.person = function(){
  return Players.findOne({ _id: Session.get('player_id') });
};







