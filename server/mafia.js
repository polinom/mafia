

Meteor.methods({
    create_game: function(citizens, mafias, doctors, sheriffs){
        var game = true;
        var code;
        while (game) {
            code = Random.hexString(4);
            game = Games.findOne({code: code});
        }
        if (!game){
            Games.insert({
                          code: code, citizens: citizens,
                          mafias: mafias, doctors: doctors,
                          sheriffs: sheriffs, active_players: 0,
                          game_state: 'waiting',
                      });
        }
        return code;
    },

   assign_roles: function(game){
     var citizens = _.range(game.citizens).map(function(){return 'citizen';});
     var sheriffs = _.range(game.sheriffs).map(function(){return 'sheriff';});
     var mafias = _.range(game.mafias).map(function(){return 'mafia';});
     var doctors = _.range(game.doctors).map(function(){return 'doctor';});
     var list_all = citizens.concat(sheriffs).concat(mafias).concat(doctors);
     Players.find({code: game.code}).forEach(function (player) {
         var role = list_all.splice(list_all.indexOf(Random.choice(list_all)),1)[0];
         Players.update({_id: player._id}, {$set: {role: role}});
    });
   },

   check_all_vote_ready: function(code){
    console.log(Players.all_ready_night(code));
    if (Players.all_ready_vote(code)) {
      Games.update({code: code}, {$set: {game_state: 'vote'}});
    }
  },

   check_all_night_ready: function(code){
    if (Players.all_ready_night(code)){
      Games.update({code: code}, {$set: {game_state: 'night'}});
    }
   },

   check_all_voted_day: function(code){
    if (Players.is_all_voted_day()){
      Games.update({code: code}, {game_state: 'results'});
      var results = Players.find({code: code, alive: true}).map(function(player){
           return {'player_id': player._id, votes: player.voters_list.length};
      });
      
    }
   }

});